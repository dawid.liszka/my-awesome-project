const createApplication = require('./src/application')

try {
    createApplication()
} catch (error) {
    console.error(`Error occured while creating application : ${error}`)
}
