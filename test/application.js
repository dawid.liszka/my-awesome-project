const request = require('supertest');

const createApplication = require('../src/application');

let application

beforeEach(() => {
    application = createApplication()
})

afterEach(() => {
    application.close()
})

describe('GET /', function() {
    it('responds with json', function(done) {
      request(application)
        .get('/')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
});
