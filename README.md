# Presentations repository

Hey, I'd like to present my presentations repository that covers preparation, configuration, explanations with practical examples of various topics

## What was done and you can check out

*  [CI/CD (Auto DevOps) configuration using Gitlab](https://gitlab.com/dawid.liszka/my-awesome-project/tree/cicd)
*  [High uptime application using Kubernetes from the footprint level](https://gitlab.com/dawid.liszka/my-awesome-project/tree/uptime)
*  [Load test configuration using Locust with examples](https://gitlab.com/dawid.liszka/my-awesome-project/tree/stress)