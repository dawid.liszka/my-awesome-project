FROM node:carbon-alpine
WORKDIR /application

COPY package*.json ./

RUN npm install

COPY . .

CMD npm run start:production
