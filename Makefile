build:
	docker build -t hello .

run:
	docker run -d -p 5000:5000 --name hello hello

stop:
	docker kill hello
	docker rm hello

logs:
	docker logs hello