const express = require('express')

function createApplication() {
    const application = express()
    const port = 5000

    application.get('/', (req, res) => res.json({ message: 'Hello World!' }))
    
    return application.listen(port, () => console.log(`Application listening on port ${port}!`))
}

module.exports = createApplication
